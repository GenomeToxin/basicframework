package libraries;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverManager {

    private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();

    public static synchronized WebDriver getDriver() {
        return webDriver.get();
    }

    public static synchronized WebDriver setDriver(String browser) {
        WebDriver driver = null;

        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--whitelisted-ips");
        options.addArguments("--disable-gpu");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-infobars");
        options.addArguments("--disable-extensions");
        options.addArguments("--disable-plugins");
        options.addArguments("--disable-popup-blocking");
        options.addArguments("incognito");
        options.addArguments("start-maximized");
        options.addArguments("--disable-browser-side-navigation");
        options.addArguments("--log-level=2");

        options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        options.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);

        options.setPageLoadStrategy(PageLoadStrategy.NONE);
        driver = new ChromeDriver(options);

        System.setProperty("webdriver.chrome.verboseLogging", "true");

        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        System.out.println("Running in " + browser + " version: " + cap.getVersion());

        return driver;
    }

    public static synchronized void setWebDriver(String browser, String url) {
        webDriver.set(setDriver(browser));
        WebDriver driver = getDriver();
        driver.navigate().to(url);
    }

    public synchronized void removeDriver() throws NoSuchSessionException {
        WebDriver driver = getDriver();
        if (driver !=null) {
            driver.manage().deleteAllCookies();
            driver.quit();
            webDriver.remove();
        }
    }
}

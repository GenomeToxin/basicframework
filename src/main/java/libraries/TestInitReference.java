package libraries;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;

public class TestInitReference {

    private String BROWSER = "Chrome";
    private String URL = "https://nightly35.compliancedesktop.com";

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(Method method){
        DriverManager.setWebDriver(BROWSER, URL);
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult result) throws Exception {
//		new DriverManager().removeDriver();
    }

}
